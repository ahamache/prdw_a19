# coding: utf8

import csv
import numpy as np
from sklearn.model_selection import KFold
from sklearn.externals import joblib
import time

def calc_erreur(test_label, test_predic):
    N=len(test_label)
    e=0
    for i in range(0,N): 
        if (test_label[i]!=test_predic[i]):
            e=e+1
    return (e/N)

data_path = './data/extracted_features/tr_mean_sdev.csv'
data_path1 = './data/extracted_features/stable_grp_size.csv'

tr_mean=np.loadtxt("./data/extracted_features/tr_mean_sdev.csv",dtype=float,delimiter=',')
stable=np.loadtxt("./data/extracted_features/stable_grp_size.csv",dtype=int,delimiter=',')
diff_in_out=np.loadtxt("./data/extracted_features/diff_in_out_tr.csv",dtype=float,delimiter=',')

N=len(tr_mean)

data_path = './data/generated_numbers_all.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    # get header from first row
    headers = next(reader)
    # get all the rows as a list
    phone_numbers = list(reader)

numbers=np.zeros((N,8),float)

num=np.zeros((len(phone_numbers),2),int)
k=0
for i in phone_numbers:
    num[k][0]=i[0]
    if i[3]=='true':
        num[k][1]=1
    else:
        num[k][1]=0
    k=k+1

labels=np.zeros((N,1),int)
P=int(2*N/3)
train_data=np.zeros((P,7),float)
test_data=np.zeros((N-P,7),float)

for j in range(0,N):
    numbers[j][0]=tr_mean[j][0]
    numbers[j][1]=tr_mean[j][1]
    numbers[j][2]=tr_mean[j][2]
    numbers[j][3]=tr_mean[j][3]
    numbers[j][4]=tr_mean[j][4]
    numbers[j][5]=stable[j][1]
    numbers[j][6]=diff_in_out[j][1]
    numbers[j][7]=diff_in_out[j][2]
    if (j<P):
        train_data[j][0]=tr_mean[j][1]
        train_data[j][1]=tr_mean[j][2]
        train_data[j][2]=tr_mean[j][3]
        train_data[j][3]=tr_mean[j][4]
        train_data[j][4]=stable[j][1]
        train_data[j][5]=diff_in_out[j][1]
        train_data[j][6]=diff_in_out[j][2]
    else:
        test_data[j-P][0]=tr_mean[j][1]
        test_data[j-P][1]=tr_mean[j][2]
        test_data[j-P][2]=tr_mean[j][3]
        test_data[j-P][3]=tr_mean[j][4]
        test_data[j-P][4]=stable[j][1]
        test_data[j-P][5]=diff_in_out[j][1]
        test_data[j-P][6]=diff_in_out[j][2]

for j in range(0,N):
    for i in range(0,len(num)):
        if num[i][0]==numbers[j][0]:
            labels[j]=num[i][1]
            #numbers[j][8]=num[i][1]

#for i in numbers:
#    print(i)

train_label=labels[0:P]
test_label=labels[P+1:N-1]
#print(len(train_data))
#print(len(train_label))

time0=time.time()

print("SVC")
from sklearn.svm import SVC
clf = SVC()
clf.fit(train_data, np.ravel(train_label))
preda = clf.predict(test_data)
err=calc_erreur(test_label,preda)#SVC
print("l'erreur vaut :")
print(err)
np.savetxt("./data_classifiers/predictionSVC_numbers.txt",preda)
#np.savetxt("labels.txt",test_label)

joblib.dump(clf, './data_classifiers/Classifiers/classifier_numbers_SVC.pkl')

time1=time.time()

print("Random Forest")
from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(n_estimators=8)
clf.fit(train_data, np.ravel(train_label), sample_weight=None)
preda = clf.predict(test_data)
err=calc_erreur(test_label,preda)#Random Forest
print("l'erreur vaut :")
print(err)
np.savetxt("./data_classifiers/predictionRandomForest_numbers.txt",preda)

joblib.dump(clf, './data_classifiers/Classifiers/classifier_numbers_RandomForest.pkl')

time2=time.time()

print("AdaBoost")
from sklearn.ensemble import AdaBoostClassifier
clf = AdaBoostClassifier(None,50)
clf.fit(train_data, np.ravel(train_label))
preda = clf.predict(test_data)
err=calc_erreur(test_label,preda)#AdaBoost
print("l'erreur vaut :")
print(err)
np.savetxt("./data_classifiers/predictionAdaBoost_numbers.txt",preda)

joblib.dump(clf, './data_classifiers/Classifiers/classifier_numbers_AdaBoost.pkl')

time3=time.time()


print("DecisionTree")
from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier()
clf.fit(train_data, train_label)
preda = clf.predict(test_data)
err=calc_erreur(test_label,preda)#Decision Tree
print("l'erreur vaut :")
print(err)
np.savetxt("./data_classifiers/predictionDecisionTree_numbers.txt",preda)

joblib.dump(clf, './data_classifiers/Classifiers/classifier_numbers_DecisionTree.pkl')

time4=time.time()


print("KNeighbors")
from sklearn.neighbors import KNeighborsClassifier
clf = KNeighborsClassifier()
clf.fit(train_data, np.ravel(train_label), )
preda = clf.predict(test_data)
err=calc_erreur(test_label,preda)#KNeighbors
print("l'erreur vaut :")
print(err)
np.savetxt("./data_classifiers/predictionKNeighbors_numbers.txt",preda)

joblib.dump(clf, './data_classifiers/Classifiers/classifier_numbers_KNeighbors.pkl')

time5=time.time()

f1=open("./time_classifier_number.txt",'w+')
f1.write("SVC : %s\n" % (time1-time0))
f1.write("Random Forest : %s\n" % (time2-time1))
f1.write("AdaBoost : %s\n" % (time3-time2))
f1.write("Decision Tree : %s\n" % (time4-time3))
f1.write("KNeighbors : %s\n" % (time5-time4))

#print(time0)
#print(time1)
#print(time2)
#print(time3)
#print(time4)
#print(time5)

print('end')
