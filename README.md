Instructions for deployment of project.

# I. INSTALL TIGERGRAPH
We used a Developer Edition, for that we downloaded directly the Linux image with built-in TigerGraph.
https://www.tigergraph.com/developer/
Download the virtual image for VirtualBox.
### Set up TigerGraph
##### Create Graph and Graph Schema
The _create_schema.gsql_ script creates the graph and defines the schema associated to the graph.
```sh
$ cd src_tigergraph && gsql create_schema.gsql
```
##### Create Loading Jobs
The _create_loading_jobs.gsql_ script creates and install Loading Jobs in database.
```sh
$ cd src_tigergraph && gsql create_loading_jobs.gsql
```
##### Create Requests
The _create_requests.gsql_ script creates and install some requests in database for data visualization and feature extraction.
```sh
$ cd src_tigergraph && gsql create_requests.gsql
```

# II. GENERATE PASSED DATA
### Generate clean numbers
Data is generated under _data/generated_numbers_clean.csv_.
```sh
$ cd src_generators && python generate_numbers_clean.py
```

### Generate fraudulent numbers
```sh
$ cd src_generators && python generate_numbers_frauds.py
```
Data is generated under _data.generated_numbers_fraudulous.csv_.

### Generate clean transactions
```sh
$ cd src_generators && python generate_transactions_clean.py
```
The script generates transactions between clean numbers only.
Data is generated under _data/generated_transactions_clean.csv_.
### Generate fraud 1 type transactions
```sh
$ cd src_generators && python generate_transactions_frauds1.py
```
The script generates transactions made from all numbers (clean + fraudulous) and received by fraudulent numbers only.
Data is generated under _data/generated_transactions_frauds1.csv_.
###### WARNING
The script uses the file data “generated_numbers_all.csv” to have the list of all numbers (clean + fraudulent). However this file is not updated automatically after generating new numbers, so it should be updated manually every time one of the scripts “generate_numbers_clean.py” or “generate_numbers_frauds.py” is called.

# LOAD PASSED DATA INTO TIGERGRAPH
### Load all generated data into database
This loads everything into the database : sim_cards, phone_calls, transactions, communications, both fraudulent and clean.
```sh
$ gsql -g cdr run loading job load_data using f="./data/generated_transactions_clean.csv"
$ gsql -g cdr run loading job load_data using f="./data/generated_transactions_frauds1.csv"
```
### Train the classifiers for the detection
After the passed data were loaded into the database, this script will train the classifiers to set up the detection. It will also produce a text file with the time that take each classifier to be trained and the percentage of errors. This file is _./time_classifier_transaction.txt_.
```sh
$ python classifier_transactions.py &
```
In a possible way of improving, there is also a script to train data to detect fraudulent phone numbers that is not used yet. 
```sh
$ python classifier_numbers.py &
```

### Load only sim_cards into database
This is a necessary step before real-time detection.
```sh
$ gsql -g cdr run loading job load_sim_cards using f="./data/generated_numbers_all.csv"
```

# INSTALL AND SET UP KAFKA

Install Zookeeper and Kafka in your Linux Environment. We followed the steps described in https://openclassrooms.com/fr/courses/4451251-gerez-des-flux-de-donnees-temps-reel/4451521-metamorphosez-vos-applications-temps-reel-avec-kafka

### Terminal Window 1 ###
Run ZooKeeper :
```sh
$ cd [kafka directory]
bin/zookeeper-server-start.sh config/zookeeper.properties
```
### Terminal Window 2 ###
Run Kafka :
```sh
$ cd [kafka directory]
bin/kafka-server-start.sh config/server.properties
```

### Terminal Window 3 ###
Create Kafka topic :
```sh
$ ./bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic blabla
```

### Setup Kafka For TigerGraph
```sh
$ cd src_tigergraph && gsql kafka_create_data_source.gsql
---this creates Kafka data source in TigerGraph
```
# START REAL TIME GENERATION AND DETECTION
### Generate real time data
The real time data generator scripts generate random communications (calls, sms associated or not associated to transactions). If the communication is associated to a transaction, the classifier is called in realtime to predict if transaction is a fraud or not.
```sh
$ ./run_generators_realtime.sh
```

However, the function that calls the classifier is in a separate python script, so it can be called from any python program.

### Call the classifier from a python program
```python
from call_realtime_classifier import call_classifier
#[......]
fraud_boolean = call_classifier(amount,emitter_number,receiver_number)
```

The real time data generators write directly in the kafka topic. In order to load data from Kafka to Tigergraph, the Kafka Loading Job must be ran as following.

### Run Kafka Loading Job
```sh
$ cd src_tigergraph && gsql kafka_run_loading_job.gsql
```




