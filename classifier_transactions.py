# coding: utf8

import csv
import numpy as np
from sklearn.model_selection import KFold
from sklearn.externals import joblib
import time

def calc_erreur(test_label, test_predic):
    N=len(test_label)
    e=0
    for i in range(0,N): 
        if (test_label[i]!=test_predic[i]):
            e=e+1
    return (e/N)

tr_mean=np.loadtxt("./data/extracted_features/tr_mean_sdev.csv",dtype=float,delimiter=',')
stable=np.loadtxt("./data/extracted_features/stable_grp_size.csv",dtype=int,delimiter=',')

data_path = './data/generated_transactions_clean.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    # get header from first row
    headers = next(reader)
    # get all the rows as a list
    transac = list(reader)

transac0=[]
for i in transac:
    if i[11]!='':
        transac0.append(i)
t1=len(transac0)
#print(t1)
#print(transac0)

data_path = './data/generated_transactions_frauds1.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    # get header from first row
    headers = next(reader)
    # get all the rows as a list
    transac1 = list(reader)
t2=len(transac1)#-t1

#for i in transac:
#    print(i)

N=len(transac0)+len(transac1)

data=np.zeros((N,7),float)
label=np.zeros((N,1),float)
k=0
#for i in transac:
for i in range(0,t1):
    label[k][0]=0
    #print(type(transac[i][11]))
    data[k][0]=float(transac0[i][11])
    #print(transac[i][11])
    for j in range(0,len(tr_mean)):
        #print(tr_mean[j][0])
        if (int(tr_mean[j][0])==int(transac0[i][3])): #emetteur de la transaction
            data[k][5]=tr_mean[j][2]
            data[k][6]=tr_mean[j][4]
            #print("boucle 1, if 1")
        if (int(tr_mean[j][0])==int(transac0[i][7])): #destinataire de la transaction
            data[k][2]=tr_mean[j][1]
            data[k][3]=tr_mean[j][3]
            #print("boucle 1, if 2")
    for j in range(0,len(stable)):
        #print(stable[j][0])
        if (int(stable[j][0])==int(transac0[i][3])): #emetteur de la transaction
            data[k][4]=stable[j][1]
            #print("boucle 2, if 1")
        if (int(stable[j][0])==int(transac0[i][7])): #destinataire de la transaction
            data[k][1]=stable[j][1]
            #print("boucle 2, if 2")
    k=k+1

for i in range(0,t2):
    label[k][0]=1
    data[k][0]=transac1[i][11]
    #print(transac[i][11])
    #print(data[k][0])
    #print(transac[i][3])
    for j in range(0,len(tr_mean)):
        #print(tr_mean[j][0])
        if (int(tr_mean[j][0])==int(transac1[i][3])): #emetteur de la transaction
            data[k][5]=tr_mean[j][2]
            data[k][6]=tr_mean[j][4]
            #print("boucle 1, if 1")
        if (int(tr_mean[j][0])==int(transac1[i][7])): #destinataire de la transaction
            data[k][2]=tr_mean[j][1]
            data[k][3]=tr_mean[j][3]
            #print("boucle 1, if 2")
    for j in range(0,len(stable)):
        #print(stable[j][0])
        if (int(stable[j][0])==int(transac1[i][3])): #emetteur de la transaction
            data[k][4]=stable[j][1]
            #print("boucle 2, if 1")
        if (int(stable[j][0])==int(transac1[i][7])): #destinataire de la transaction
            data[k][1]=stable[j][1]
            #print("boucle 2, if 2")
    k=k+1

#print(data)
x1=0
x2=int(2*t1/3)
x3=x2+1
x4=t1+1
y1=t1+1
y2=t1+int(2*t2/3)
y3=y2+1
y4=t1+t2
#print('t1=')print(t1)print('t2=')print(t2)print('x1=')print(x1)print('x2=')print(x2)print('x3=')print(x3)print('x4=')print(x4)print('y1=')print(y1)print('y2=')print(y2)print('y3=')print(y3)print('y4=')print(y4)

train_data=np.zeros((x2+y2-t1+1,7),float)
train_label=np.zeros((x2+y2-t1+1,1),float)
for i in range(x1,x3):
    for j in range(0,7):
        train_data[i][j]=data[i][j]
    train_label[i]=label[i]
p=x3
#print('fin premiere boucle')
for i in range(y1,y3):
    for j in range(0,7):
        train_data[p][j]=data[i][j]
    train_label[p]=label[i]
    #print(p)
    p=p+1
    #print(i)
#print(data)
#print(train_data)
#print(train_label)

test_data=np.zeros((x4-x3+y4-y3,7),float)
test_label=np.zeros((x4-x3+y4-y3,1),float)

p=0
#print("taille de test :")
#print(x4-x3+y4-y3)
for i in range(x3,x4):
    for j in range(0,7):
        test_data[p][j]=data[i][j]
    test_label[p]=label[i]
    #print(p)
    p=p+1
#print('fin premiere boucle')
for i in range(y3,y4):
    for j in range(0,7):
        test_data[p][j]=data[i][j]
    test_label[p]=label[i]
    #print(p)
    p=p+1

#print(test_data)
#print(test_label)
#print(len(train_label))
#print(len(test_label))

time0=time.time()

#print("SVC")
#from sklearn.svm import SVC
#clf = SVC()
#clf.fit(train_data, np.ravel(train_label))
#preda = clf.predict(test_data)
#err1=calc_erreur(test_label,preda)#SVC
#print("l'erreur vaut :")
#print(err1)
#np.savetxt("./data_classifiers/predictionSVC_transactions.txt",preda)
#np.savetxt("labels.txt",test_label)

#joblib.dump(clf, './data_classifiers/Classifiers/classifier_transactions_SVC.pkl')

time1=time.time()

print("Random Forest")
from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(n_estimators=8)
clf.fit(train_data, np.ravel(train_label), sample_weight=None)
preda = clf.predict(test_data)
err2=calc_erreur(test_label,preda)#Random Forest
print("l'erreur vaut :")
print(err2)
np.savetxt("./data_classifiers/predictionRandomForest_transactions.txt",preda)

joblib.dump(clf, './data_classifiers/Classifiers/classifier_transactions_RandomForest.pkl')

time2=time.time()

print("AdaBoost")
from sklearn.ensemble import AdaBoostClassifier
clf = AdaBoostClassifier(None,50)
clf.fit(train_data, np.ravel(train_label))
preda = clf.predict(test_data)
err3=calc_erreur(test_label,preda)#AdaBoost
print("l'erreur vaut :")
print(err3)
np.savetxt("./data_classifiers/predictionAdaBoost_transactions.txt",preda)

joblib.dump(clf, './data_classifiers/Classifiers/classifier_transactions_AdaBoost.pkl')

time3=time.time()

print("DecisionTree")
from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier()
clf.fit(train_data, train_label)
preda = clf.predict(test_data)
err4=calc_erreur(test_label,preda)#Decision Tree
print("l'erreur vaut :")
print(err4)
np.savetxt("./data_classifiers/predictionDecisionTree_transactions.txt",preda)

joblib.dump(clf, './data_classifiers/Classifiers/classifier_transactions_DecisionTree.pkl')

time4=time.time()

print("KNeighbors")
from sklearn.neighbors import KNeighborsClassifier
clf = KNeighborsClassifier()
clf.fit(train_data, np.ravel(train_label), )
preda = clf.predict(test_data)
err5=calc_erreur(test_label,preda)#KNeighbors
print("l'erreur vaut :")
print(err5)
np.savetxt("./data_classifiers/predictionKNeighbors_transactions.txt",preda)

joblib.dump(clf, './data_classifiers/Classifiers/classifier_transactions_KNeighbors.pkl')

time5=time.time()

f1=open("./time_classifier_transaction.txt",'w+')
#f1.write("SVC : %s - erreur : %d pourcents \n" % (time1-time0,err1))
f1.write("Random Forest : %s - erreur : %d pourcents \n" % (time2-time1,err2))
f1.write("AdaBoost : %s - erreur : %d pourcents \n" % (time3-time2,err3))
f1.write("Decision Tree : %s - erreur : %d pourcents \n" % (time4-time3,err4))
f1.write("KNeighbors : %s - erreur : %d pourcents \n" % (time5-time4,err5))


print('end')


