#!/bin/bash

cd ./src_tigergraph

gsql ./create_schema.gsql

gsql ./create_loading_jobs.gsql

gsql ./create_requests.gsql

gsql -g cdr run loading job load_data using f="./data/generated_transactions_clean.csv"

gsql -g cdr run loading job load_data using f="./data/generated_transactions_frauds1.csv"

gsql ./kafka_create_data_source.gsql

gsql ./kafka_create_loading_job.gsql
