# coding: utf8

import csv
import random
import numpy as np
import time
from kafka import KafkaProducer
from call_realtime_classifier import call_classifier

N=600

producer=KafkaProducer(bootstrap_servers="localhost:9092")

data_path = '../data/generated_numbers_clean.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    # get header from first row
    headers = next(reader)
    # get all the rows as a list
    phone_numbers = list(reader)

def create_date():
    (year,month,day,hour,minute,second,x,y,z)=time.localtime()
    return (day,month,year,hour,minute,second)

def create_event():
    i=random.randrange(1,3,1)
    if i==1:
        return ('CALL',create_duration())
    else :
        return ('SMS',0)

def create_duration():
    return random.randrange(1,10801,1)

def create_numbers():
    n=len(phone_numbers)
    i=random.randrange(0,n,1)
    j=random.randrange(0,n,1)
    while(j==i):
        j=random.randrange(1,n+1,1)
    return (phone_numbers[i],phone_numbers[j])
    
def formatage(num):
    if len(num)==1:
        num.append('')
    if len(num)==2:
        num.append('')
    return num

def create_transaction():
    i=random.randrange(1,3,1)
    if i==1:
        return ''
    else:
        return ("%d.%d" % (random.randrange(1,N+1,1),random.randrange(0,100,1)))

while(1):
    (d,m,y,h,minu,s)=create_date()
    (event,duration)=create_event()
    (appelant,appele)=create_numbers()
    appelant=formatage(appelant)
    appele=formatage(appele)
    tr = create_transaction()
    if tr=='':
        flag=0
    else:
        flag=call_classifier(tr,appelant[0],appele[0])

    stri = "%s,%d-%d-%d %d:%d:%d,%d,%s,%s,%s,%s,\n" % (event,y,m,d,h,minu,s,duration,appelant[0],appele[0],tr,flag)
    print(stri)
    
    producer.send("test1",stri)
    #On attend entre 1 seconde et 5 secondes entre deux transactions
    stime=random.randrange(1,5,1)
    time.sleep(stime)


