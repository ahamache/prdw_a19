# coding: utf8

import csv
import random
import numpy as np
import time
from kafka import KafkaProducer
from call_realtime_classifier import call_classifier

N=600

producer=KafkaProducer(bootstrap_servers="localhost:9092")

data_path = '../data/generated_numbers_fraudulous.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    # get header from first row
    headers = next(reader)
    # get all the rows as a list
    phone_numbers = list(reader)

def create_date():
    (year,month,day,hour,minute,second,x,y,z)=time.localtime()
    return (day,month,year,hour,minute,second)

def create_event():
    i=random.randrange(1,3,1)
    if i==1:
        return ('CALL',create_duration())
    else :
        return ('SMS',0)

def create_duration():
    return random.randrange(1,10801,1)

def create_numbers():
    n=len(phone_numbers)
    t=random.randrange(0,n,1)
    return (phone_numbers[t])
    
def formatage(num):
    if len(num)==1:
        num.append('')
    if len(num)==2:
        num.append('')
    return num

def create_transaction():
    return ("%d.%d" % (random.randrange(1,N+1,1),random.randrange(0,100,1)))

def create_biaise(nb_transac):
    destinataire=create_numbers()
    destinataire=formatage(destinataire)
    for i in range(0,nb_transac):
        (d,m,y,h,minu,s)=create_date()
        (event,duration)=create_event()
        source=create_numbers()
        source=formatage(source)
        if source[0]!=destinataire[0]:
            tr = create_transaction()
            if tr=='':
                flag=0
            else:
                flag=call_classifier(tr,source[0],destinataire[0])
            stri = ("%s,%d-%d-%d %d:%d:%d,%d,%s,%s,%s,%s\n" % (event,y,m,d,h,minu,s,duration,source[0],destinataire[0],tr,flag))
            print("biaised_1 generation : "+ stri)
            producer.send("test1",stri)
        k=int(120/nb_transac)+1
        stime=random.randrange(1,k,1)
        #On attend un nombre aléatoire de secondes entre la génération de deux transactions d'un même lot
        #de telle sorte que l'ensemble des transactions d'un même lot soit généré en maximum 2min
        time.sleep(stime)

while(1):
    nb_transac = random.randrange(10,101,1)
    create_biaise(nb_transac)
    #On attend entre 5min et 10min pour générer un nouveau lot de transactions biaisées
    time.sleep(random.randrange(300,601,1))
