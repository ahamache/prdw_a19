# coding: utf8

#a stable group (array of phone numbers) is associated to each phone number
#we consider that each number has the same stable group for all year 2019
#for each month of year 2019 and for each number, we generate a random number of communications between the unmber and each number of its stable group
#at the end we generate totally random communications between any of the numbers, at any date of 2019

import csv
import random
import numpy as np

N=600

data_path = '../data/generated_numbers_clean.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    # get header from first row
    headers = next(reader)
    # get all the rows as a list
    phone_numbers = list(reader)



def create_date():
    month=random.randrange(1,13,1)
    if (month in {1,3,5,7,8,10,12}):
        day=random.randrange(1,32,1)
    if (month==2):
        day=random.randrange(1,29,1)
    else:
        day=random.randrange(1,31,1)
    year=2019
    hour=random.randrange(0,24,1)
    minute=random.randrange(0,60,1)
    second=random.randrange(0,60,1)
    return (day,month,year,hour,minute,second)

def create_event():
    i=random.randrange(1,3,1)
    if i==1:
        return ('CALL',create_duration())
    else :
        return ('SMS',0)

def create_duration():
    return random.randrange(1,10801,1)

def get_number():
    n=len(phone_numbers)
    i=random.randrange(0,n,1)
    return (phone_numbers[i])

def create_transaction():
    i=random.randrange(1,3,1)
    if i==1:
        return ''
    else:
        return ("%d.%d" % (random.randrange(1,N+1,1),random.randrange(0,100,1)))

#print('Donner le nombre de groupes stables à générer')
#nb_transac = int(input())

f1=open("../data/generated_transactions_clean.csv",'w+')

def generate_transaction(appelant,appele):
    (d,m,y,h,minu,s)=create_date()
    y = 2019
    for m in range(1,13,1):
        t=random.randrange(2,16,1) 
        for k in range(0,t):
            if (m in {1,3,5,7,8,10,12}):
                d=random.randrange(1,32,1)
            if (m==2):
                d=random.randrange(1,29,1)
            else:
                d=random.randrange(1,31,1)
            h=random.randrange(0,24,1)
            minu=random.randrange(0,60,1)
            s=random.randrange(0,60,1)
            (event,duration)=create_event()
            call_direction = random.randrange(0,2,1)
            if call_direction==1:
                f1.write("%s,%d-%d-%d %d:%d:%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,false,\n" % (event,y,m,d,h,minu,s,duration,appelant[0],appelant[1],appelant[2],appelant[3],appele[0],appele[1],appele[2],appele[3],create_transaction()))
            else:
                f1.write("%s,%d-%d-%d %d:%d:%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,false,\n" % (event,y,m,d,h,minu,s,duration,appele[0],appele[1],appele[2],appele[3],appelant[0],appelant[1],appelant[2],appelant[3],create_transaction()))

def create_groupe_stable(p):
    n=random.randrange(2,7,1)
    #un groupe stable sera composé de 2 à 6 personnes
    comptes=[]
    comptes.append(phone_numbers[p])
    for i in range(1,n):
        comptes.append(get_number())
    for i in range(1,n):
        generate_transaction(comptes[0],comptes[i])


for p in range(0,len(phone_numbers)):
    create_groupe_stable(p)
for t in range(0,3*len(phone_numbers)):
    (d,m,y,h,minu,s)=create_date()
    (event,duration)=create_event()
    appelant=get_number()
    appele=get_number()
    f1.write("%s,%d-%d-%d %d:%d:%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,false,\n" % (event,y,m,d,h,minu,s,duration,appelant[0],appelant[1],appelant[2],appelant[3],appele[0],appele[1],appele[2],appele[3],create_transaction()))
    

f1.close()


