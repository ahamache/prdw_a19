# coding: utf8

import csv
import random
import numpy as np

N=600

data_path = '../data/generated_numbers_all.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    # get header from first row
    headers = next(reader)
    # get all the rows as a list
    phone_numbers = list(reader)

data_path = '../data/generated_numbers_fraudulous.csv'
with open(data_path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    # get header from first row
    headers = next(reader)
    # get all the rows as a list
    fraud_numbers = list(reader)

def create_date():
    hour=random.randrange(0,24,1)
    minute=random.randrange(0,60,1)
    second=random.randrange(0,60,1)
    return (hour,minute,second)

def create_event():
    i=random.randrange(1,3,1)
    if i==1:
        return ('CALL',create_duration())
    else :
        return ('SMS',0)

def create_duration():
    return random.randrange(1,10801,1)

def get_number():
    n=len(phone_numbers)
    t=random.randrange(0,n,1)
    return (phone_numbers[t])

def create_fraud_numbers():
    n=len(fraud_numbers)
    t=random.randrange(0,n,1)
    return (fraud_numbers[t])

def create_transaction():
    return ("%d.%d" % (random.randrange(1,N+1,1),random.randrange(0,100,1)))

def create_biaise(nb_transac, destinataire):
    y=2019
    m=random.randrange(1,13,1)
    if (m in {1,3,5,7,8,10,12}):
        d=random.randrange(1,32,1)
    if (m==2):
        d=random.randrange(1,29,1)
    else:
        d=random.randrange(1,31,1)
    #destinataire=create_fraud_numbers()
    for i in range(0,nb_transac):
        (h,minu,s)=create_date()
        (event,duration)=create_event()
        source=get_number()
        if source[0]!=destinataire[0]:
            f1.write("%s,%d-%d-%d %d:%d:%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,true,\n" % (event,y,m,d,h,minu,s,duration,source[0],source[1],source[2],source[3],destinataire[0],destinataire[1],destinataire[2],destinataire[3],create_transaction()))


f1=open("../data/generated_transactions_frauds1.csv",'w+')
#print('Donner le nombre de lots de transactions biasées à générer')
#nb_lot_transac = int(input())
for j in fraud_numbers:
    nb_transac = random.randrange(10,51,1)
    create_biaise(nb_transac,j)
f1.close()
