import json
import urllib2
import urllib
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib

def call_classifier(amount,emettor,receptor):
	get_emettor_pred1_url = "http://localhost:9000/query/cdr/v_get_tr_mean_sdev?p=%s" % emettor
	get_emettor_pred2_url = "http://localhost:9000/query/cdr/v_get_stable_group_size?p=%s" % emettor
	get_receptor_pred1_url = "http://localhost:9000/query/cdr/v_get_tr_mean_sdev?p=%s" % receptor
	get_receptor_pred2_url = "http://localhost:9000/query/cdr/v_get_stable_group_size?p=%s" % receptor


	emettor_pred1 = json.load(urllib2.urlopen(get_emettor_pred1_url))
	emettor_pred2 = json.load(urllib2.urlopen(get_emettor_pred2_url))
	receptor_pred1 = json.load(urllib2.urlopen(get_receptor_pred1_url))
	receptor_pred2 = json.load(urllib2.urlopen(get_receptor_pred2_url))

	#print(emettor_pred1)
	#print(emettor_pred2)
	#print(receptor_pred1)
	#print(receptor_pred2)

	emettor_pred1 = emettor_pred1["results"][0]
	emettor_pred2 = emettor_pred2["results"][0]
	receptor_pred1 = receptor_pred1["results"][0]
	receptor_pred2 = receptor_pred2["results"][0]

	matrixpredict = np.zeros((1,7))
	matrixpredict[0,0] = amount
	matrixpredict[0,1] = receptor_pred2["stable_group_size"]
	matrixpredict[0,2] = receptor_pred1["in_mean"]
	matrixpredict[0,3] = receptor_pred1["in_sdev"]
	matrixpredict[0,4] = emettor_pred2["stable_group_size"]
	matrixpredict[0,5] = emettor_pred1["out_mean"]
	matrixpredict[0,6] = emettor_pred1["out_sdev"]

	clf = joblib.load("../data_classifiers/Classifiers/classifier_transactions_RandomForest.pkl")

	flag = clf.predict(matrixpredict)

	print(flag[0])
	return(flag[0])